/*
 * @Author: 常远 469764341@qq.com
 * @Date: 2023-12-17 19:48:16
 * @LastEditors: 常远 469764341@qq.com
 * @LastEditTime: 2023-12-26 16:06:31
 * @FilePath: \agent-world\ui-admin\src\preload.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

import tileset from "./assets/v2.png"
import mapJson from "./assets/GPTRPGMap.json"
import characters from "./assets/characters.png"

export default function preload() {
  this.load.image("tiles", tileset, {
    frameWidth: 16,
    frameHeight: 16,
  });
  this.load.tilemapTiledJSON("field-map", mapJson);
  this.load.spritesheet("player", characters, {
    frameWidth: 26,
    frameHeight: 36,
  });
  this.load.spritesheet("player2", characters, {
    frameWidth: 26,
    frameHeight: 36,
  });
  
  this.load.spritesheet("plant", tileset, {
    frameWidth: 16,
    frameHeight: 16,
  });
}
