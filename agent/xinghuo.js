import CryptoJs from 'crypto-js'
import WebSocket from 'ws'
import env from "./env.json" assert { type: "json" };

// 星火大模型的APPID
let requestObj = {
    APPID: env.APPID,
    APISecret: env.APISecret,
    APIKey: env.APIKey,
    Uid: 'changyuan',
    sparkResult: ''
}

// 向星火讯飞大模型发送消息
const sendMsg = async (questionInput) => {
    // 获取请求地址
    let myUrl = await getWebsocketUrl();
    // 获取输入框中的内容
    let inputVal = questionInput;
    // 每次发送问题 都是一个新的websocket请求
    let socket = new WebSocket(myUrl);
    
    return new Promise((resovle, reject) => {
    // 监听websocket的各阶段事件 并做相应处理
    socket.addEventListener('open', (event) => {
        console.log('开启连接！！');
        // 发送消息
        let params = {
            "header": {
                "app_id": requestObj.APPID,
                "uid": requestObj.Uid
            },
            "parameter": {
                "chat": {
                    // "domain": "general",
                    "domain": "generalv2",
                    "temperature": 0.5,
                    "max_tokens": 1024,
                }
            },
            "payload": {
                "message": {
                    // 如果想获取结合上下文的回答，需要开发者每次将历史问答信息一起传给服务端，如下示例
                    // 注意：text里面的所有content内容加一起的tokens需要控制在8192以内，开发者如有较长对话需求，需要适当裁剪历史信息
                    "text": [
                        //{ "role": "user", "content": "你是谁" }, //# 用户的历史问题
                        //{ "role": "assistant", "content": "我是AI助手" },  //# AI的历史回答结果
                        // ....... 省略的历史对话
                        { "role": "user", "content": inputVal },  //# 最新的一条问题，如无需上下文，可只传最新一条问题
                    ]
                }
            }
        };
        console.log("发送消息");
        socket.send(JSON.stringify(params))
    })

    socket.addEventListener('message', (event) => {
        
        let data = JSON.parse(event.data)
        console.log('收到消息！！');
        requestObj.sparkResult += data.payload.choices.text[0].content;
        if (data.header.code !== 0) {
            console.log("出错了", data.header.code, ":", data.header.message);
            // 出错了"手动关闭连接"
            socket.close()
        }
        if (data.header.code === 0) {
            // 对话已经完成
            if (data.payload.choices.text && data.header.status === 2) {
                setTimeout(() => {
                    // "对话完成，手动关闭连接"
                    console.log("对话完成", requestObj.sparkResult);
                    socket.close()
                }, 1000)
            }
        }
    })

    socket.addEventListener('close', (event) => {
        console.log('连接关闭！！返回结果...');
        //返回大模型结果的关键函数
        resovle(requestObj.sparkResult)
        // 清空输入框
        requestObj.sparkResult = ''
        questionInput = ''
    })

    socket.addEventListener('error', (event) => {
        console.log('连接发送错误！！');
    })
})

}
// 鉴权url地址
const getWebsocketUrl = () => {
    return new Promise((resovle, reject) => {
        // let url = "wss://spark-api.xf-yun.com/v1.1/chat";
        let url = "wss://spark-api.xf-yun.com/v3.1/chat";
        let host = "spark-api.xf-yun.com";
        let apiKeyName = "api_key";
        let date = new Date().toGMTString();
        let algorithm = "hmac-sha256"
        let headers = "host date request-line";
        // let signatureOrigin = `host: ${host}\ndate: ${date}\nGET /v1.1/chat HTTP/1.1`;
        let signatureOrigin = `host: ${host}\ndate: ${date}\nGET /v3.1/chat HTTP/1.1`;
        let signatureSha = CryptoJs.HmacSHA256(signatureOrigin, requestObj.APISecret);
        let signature = CryptoJs.enc.Base64.stringify(signatureSha);

        let authorizationOrigin = `${apiKeyName}="${requestObj.APIKey}", algorithm="${algorithm}", headers="${headers}", signature="${signature}"`;

        let authorization = btoa(authorizationOrigin);

        // 将空格编码
        url = `${url}?authorization=${authorization}&date=${encodeURI(date)}&host=${host}`;

        resovle(url)
    })
}

export default sendMsg;