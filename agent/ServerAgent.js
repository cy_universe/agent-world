//import { Configuration, OpenAIApi } from "openai";
import extract from "extract-json-from-string";
import sendMsg from "./xinghuo.js";

// import env from "./env.json" assert { type: "json" };

/* const configuration = new Configuration({
  apiKey: env.OPENAI_API_KEY,
}); */

//const openai = new OpenAIApi(configuration);

class ServerAgent {
  constructor(id) {
    this.id = id;
  }

  async processMessage(parsedData) {
    try {
      const prompt = `# Introduction

      You are acting as an agent living in a simulated 2 dimensional room with a door. 
      Your goal is to leave the room as fast as you can. 
      Do not go against the wall or repeatedly exploring previously visited areas.
      
      # Capabilities
      
      You have a limited set of capabilities. They are listed below:
      
      * move (up, down, left, right)
      * wait

      # Responses
      
      You must supply your responses in the form of valid JSON objects.  Your responses will specify which of the above actions you intend to take.  The following is an example of a valid response:
      
      {
        action: {
          type: "move",
          direction: "up" | "down" | "left" | "right"
        }
      }
      
      # Perceptions
      
      You will have access to data to help you make your decisions on what to do next.
      
      For now, this is the information you have access to:

      Position: 
      ${JSON.stringify(parsedData.position)}

      Surroundings:
      ${JSON.stringify(parsedData.surroundings)}

      The JSON response indicating the next move is.
      `
      const completion = await this.callOpenAI(prompt, 0);
      return completion;

    } catch (error) {
      console.error("Error processing GPT-3 response:", error);
    }
  }

  async callOpenAI(prompt, attempt) {
    if (attempt > 3) {
      return null;
    }
  
    if (attempt > 0) {
      prompt = "YOU MUST ONLY RESPOND WITH VALID JSON OBJECTS\N" + prompt;
    }

    // 调用讯飞星火模型
    const response = await sendMsg(prompt);; // 等待 fetchData 的 Promise 解析  
    console.log('LLMresponse', response); // 打印解析后的数据  
  
    const responseObject = this.cleanAndProcess(response);
    if (responseObject) {
      return responseObject;
    }
  
    return await this.callOpenAI(prompt, attempt + 1);
  }
  
  cleanAndProcess(text) {
    const extractedJson = extract(text);
    if (!extractedJson) {
      return null;
    }
    return extractedJson;
  }
}

export default ServerAgent;